import Box from "@mui/material/Box";
import Header from "../../component/header";
import DashBoardBody from "../../component/body/dashboard-body";

const Dashboard = () => {
  return (
    <Box sx={{ width: "100%", height: "100%" }}>
      <Header />
      <DashBoardBody />
    </Box>
  );
};

export default Dashboard;
