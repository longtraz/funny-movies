import Box from "@mui/material/Box";
import Header from "../../component/header";
import ShareMovieBody from "../../component/body/share-movie-body";

const ShareMovie = () => {
  return (
    <Box sx={{ width: "100%", height: "100%" }}>
      <Header />
      <ShareMovieBody />
    </Box>
  );
};

export default ShareMovie;
