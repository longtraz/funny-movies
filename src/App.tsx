import { createBrowserRouter, RouterProvider } from "react-router-dom";

import Dashboard from "./routes/dashboard";
import ShareMovie from "./routes/share-movie";

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Dashboard />,
    },
    { path: "share-movie", element: <ShareMovie /> },
  ]);
  return <RouterProvider router={router} />;
}

export default App;
