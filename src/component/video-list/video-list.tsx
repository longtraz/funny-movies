import { useState } from "react";

import Box from "@mui/material/Box";

import { Video } from "../../types";
import VideoItem from "../video-item";
import Pagination from "@mui/material/Pagination";

interface VideoListProps {
  videos: Video[];
}

const VideoList: React.FC<VideoListProps> = ({ videos }) => {
  const [page, setPage] = useState(1);

  const numberOfPage = Math.ceil(videos.length / 4);

  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };
  return (
    <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignContent: "center", gap: 2 }}>
      <Box
        sx={{
          display: " flex",
          flexWrap: "wrap",
          rowGap: [2, 2, 4, 6],
          columnGap: 6,
          justifyContent: "center",
          alignContent: "center",
        }}
      >
        {videos.slice((page - 1) * 4, (page - 1) * 4 + 4).map((video) => {
          return (
            <VideoItem url={video.url} title={video.title} source={video.source} description={video.description} />
          );
        })}
      </Box>
      {!!(numberOfPage - 1) && (
        <Pagination sx={{ alignSelf: "center" }} count={numberOfPage} page={page} onChange={handlePageChange} />
      )}
    </Box>
  );
};

export default VideoList;
