import { Link, useLocation } from "react-router-dom";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const AutheticatedSubHeader = () => {
  const location = useLocation();

  return (
    <Box sx={{ display: "flex", gap: 2 }}>
      <Typography
        variant="body1"
        noWrap
        sx={{
          display: "flex",
          alignItems: "center",
          color: "#3e4244",
          textDecoration: "none",
        }}
      >
        Welcome
      </Typography>
      {location.pathname === "/" && (
        <Button variant="outlined" color="primary" component={Link} to="/share-movie">
          Share a movie
        </Button>
      )}
      <Button variant="contained" color="primary">
        Logout
      </Button>
    </Box>
  );
};

export default AutheticatedSubHeader;
