import { useState } from "react";
import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Snackbar from "@mui/material/Snackbar";
import TextField from "@mui/material/TextField";
import CloseIcon from "@mui/icons-material/Close";

const UnauthenticatedSubHeader = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [submitValidation, setSubmitValidation] = useState({ emailValidation: true, passwordValidation: true });
  const [isSnackBarOpen, setIsSnackBarOpen] = useState(false);
  const EMAIL_FORMAT =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const validateEmail = (email: string) => {
    return email.match(EMAIL_FORMAT) !== null;
  };

  const validatePassword = (password: string) => {
    if (password.length > 5) return true;

    return false;
  };

  const generateErrorMessage = () => {
    const errorMessages = [];

    if (!submitValidation.emailValidation) errorMessages.push("Email format is not correct.");

    if (!submitValidation.passwordValidation) errorMessages.push("Password must have at least 6 digits.");

    return errorMessages.join(" ");
  };

  const handleSubmit = () => {
    const emailValidation = validateEmail(email);
    const passwordValidation = validatePassword(password);
    setSubmitValidation({
      emailValidation,
      passwordValidation,
    });
    setIsSnackBarOpen(!(emailValidation && passwordValidation));
  };

  return (
    <Box sx={{ display: "flex", gap: 2 }}>
      <Snackbar anchorOrigin={{ vertical: "top", horizontal: "right" }} open={isSnackBarOpen} autoHideDuration={3000}>
        <Alert
          action={
            <IconButton size="small" aria-label="close" color="inherit" onClick={() => setIsSnackBarOpen(false)}>
              <CloseIcon fontSize="small" />
            </IconButton>
          }
          onClose={() => setIsSnackBarOpen(false)}
          severity="error"
          sx={{ width: "100%" }}
        >
          {generateErrorMessage()}
        </Alert>
      </Snackbar>
      <TextField
        label="Email"
        variant="standard"
        size="small"
        sx={{ borderRadius: 2 }}
        value={email}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setEmail(event.target.value);
          setSubmitValidation({ emailValidation: true, passwordValidation: true });
          setIsSnackBarOpen(false);
        }}
        error={!submitValidation.emailValidation}
      />
      <TextField
        type="password"
        label="Password"
        variant="standard"
        size="small"
        sx={{ borderRadius: 2 }}
        value={password}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setPassword(event.target.value);
          setSubmitValidation({ emailValidation: true, passwordValidation: true });
          setIsSnackBarOpen(false);
        }}
        error={!submitValidation.passwordValidation}
      />
      <Button variant="contained" color="primary" onClick={handleSubmit}>
        Login/Register
      </Button>
    </Box>
  );
};

export default UnauthenticatedSubHeader;
