import { useState } from "react";

import { Link } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import HomeIcon from "@mui/icons-material/Home";
import Box from "@mui/material/Box";

import AutheticatedSubHeader from "./sub-header/authenticated-sub-header";
import UnauthenticatedSubHeader from "./sub-header/unauthenticated-sub-header";

const Header = () => {
  const [authenticated, setAuthenticated] = useState(true);

  return (
    <AppBar className="header" position="static" sx={{ width: "100%", top: 0, backgroundColor: "white" }}>
      <Container>
        <Toolbar sx={{ display: "flex", gap: 2, alignContent: "center" }}>
          <Box sx={{ display: "flex", flex: 1, alignItems: "center", gap: 2 }}>
            <Link to="/">
              <HomeIcon color="primary" />
            </Link>
            <Link to="/" style={{ textDecoration: "none" }}>
              <Typography
                variant="h6"
                noWrap
                sx={{
                  display: "flex",
                  alignItems: "center",
                  fontWeight: 900,
                  color: "#3e4244",
                  textDecoration: "none",
                }}
              >
                Funny Movies
              </Typography>
            </Link>
          </Box>
          {authenticated ? <AutheticatedSubHeader /> : <UnauthenticatedSubHeader />}
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default Header;
