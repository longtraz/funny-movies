import { useState } from "react";

import Box from "@mui/material/Box";

import { extractYoutubeVideoIdFromURL } from "../../utils/youtube";

export interface YoutubeEmbedProps extends React.HTMLAttributes<HTMLDivElement> {
  url: string;
}

const YoutubeEmbed: React.FC<YoutubeEmbedProps> = ({ url }) => {
  const embedId = extractYoutubeVideoIdFromURL(url);
  const [isLoaded, setIsLoaded] = useState(false);

  return (
    <Box sx={{ width: "100%", height: "100%" }}>
      <iframe
        onLoadCapture={() => setIsLoaded(true)}
        className="min-h-[180px] h-full w-full min-w-[320px] md:min-h-0 z-10"
        src={`https://www.youtube.com/embed/${embedId}`}
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
        title="Embedded youtube"
      />
    </Box>
  );
};

export default YoutubeEmbed;
