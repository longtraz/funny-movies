export enum VIDEO_RATING {
  UP_VOTE = "UP_VOTE",
  DOWN_VOTE = "DOWN_VOTE",
  UN_VOTE = "UN_VOTE",
}
