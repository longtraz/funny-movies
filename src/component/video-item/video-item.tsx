import { useState } from "react";

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import ThumbUpAltIcon from "@mui/icons-material/ThumbUpAlt";
import ThumbUpOffAltIcon from "@mui/icons-material/ThumbUpOffAlt";
import ThumbDownAltIcon from "@mui/icons-material/ThumbDownAlt";
import ThumbDownOffAltIcon from "@mui/icons-material/ThumbDownOffAlt";
import IconButton from "@mui/material/IconButton";

import YoutubeEmbed from "../youtube-embed";
import { VIDEO_RATING } from "./video-item.type";
import { Video } from "../../types";

const VideoItem: React.FC<Video> = ({ url, title, source, description }) => {
  const [videoRating, setVideoRating] = useState<VIDEO_RATING>(VIDEO_RATING.UN_VOTE);

  const renderRatingButton = () => {
    if (videoRating === VIDEO_RATING.UP_VOTE)
      return (
        <IconButton onClick={() => setVideoRating(VIDEO_RATING.UN_VOTE)}>
          <ThumbUpAltIcon />
        </IconButton>
      );
    if (videoRating === VIDEO_RATING.DOWN_VOTE)
      return (
        <IconButton onClick={() => setVideoRating(VIDEO_RATING.UN_VOTE)}>
          <ThumbDownAltIcon />
        </IconButton>
      );

    return (
      <Box>
        <IconButton
          onClick={() => {
            setVideoRating(VIDEO_RATING.UP_VOTE);
          }}
        >
          <ThumbUpOffAltIcon />
        </IconButton>
        <IconButton
          onClick={() => {
            setVideoRating(VIDEO_RATING.DOWN_VOTE);
          }}
        >
          <ThumbDownOffAltIcon />
        </IconButton>
      </Box>
    );
  };

  return (
    <Box
      sx={{
        display: "flex",
        gap: 2,
        justifyContent: "center",
        alignItems: "center",
        maxWidth: 600,
        width: "fit-content",
        padding: 2,
        border: "1px solid",
        borderRadius: 2,
      }}
    >
      <YoutubeEmbed url={url} />
      <Box>
        <Box sx={{ display: "flex" }}>
          <Typography
            variant="body1"
            noWrap
            sx={{
              flex: 1,
              display: "flex",
              alignItems: "center",
              color: "red",
              textDecoration: "none",
            }}
          >
            {title}
          </Typography>
          {renderRatingButton()}
        </Box>

        <Typography
          variant="body1"
          noWrap
          sx={{
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
          }}
        >
          Shared by {source}
        </Typography>
        <Typography
          variant="body1"
          noWrap
          sx={{
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
          }}
        >
          Description:
        </Typography>
        <Typography
          variant="body2"
          sx={{
            display: "flex",
            alignItems: "center",
            textDecoration: "none",
          }}
        >
          {description}
        </Typography>
      </Box>
    </Box>
  );
};

export default VideoItem;
