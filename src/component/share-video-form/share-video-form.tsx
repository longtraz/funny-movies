import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Button, TextField } from "@mui/material";

const ShareVideoForm = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        width: [400, 400, 500, 600],
        padding: 2,
        border: "1px solid",
        borderRadius: 2,
        gap: 2,
      }}
    >
      <Typography
        variant="body1"
        noWrap
        sx={{
          display: "flex",
          alignItems: "center",
          fontWeight: 900,
          color: "#3e4244",
          textDecoration: "none",
        }}
      >
        Share YouTube movies
      </Typography>
      <Box sx={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", gap: 1 }}>
        <Box sx={{ display: "flex", gap: 2, width: "100%" }}>
          <Typography
            variant="body2"
            sx={{
              display: "flex",
              width: 120,
              alignItems: "center",
              fontWeight: 900,
              color: "#3e4244",
              textDecoration: "none",
            }}
          >
            YouTube URL:
          </Typography>
          <TextField fullWidth size="small" />
        </Box>
        <Button variant="contained">Share</Button>
      </Box>
    </Box>
  );
};

export default ShareVideoForm;
