import Box from "@mui/material/Box";
import ShareVideoForm from "../../share-video-form/share-video-form";

const ShareMovieBody = () => {
  return (
    <Box
      sx={{
        display: "flex",
        width: "100%",
        height: "100%",
        paddingTop: 8,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <ShareVideoForm />
    </Box>
  );
};

export default ShareMovieBody;
