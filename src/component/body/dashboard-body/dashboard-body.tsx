import Box from "@mui/material/Box";
import { Video } from "../../../types";
import VideoList from "../../video-list";

const DashBoardBody = () => {
  const videos: Video[] = [
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
    {
      description:
        "We do not recommend using this approach in production. It requires the client to download the entire library—regardless of which components are actually used—which negatively impacts performance and bandwidth utilization.",
      url: "https://www.youtube.com/watch?v=1WKyz5x3Lx4&list=RD1WKyz5x3Lx4&start_radio=1",
      title: "Sex Gay",
      source: "abc@gmail.com",
    },
  ];
  return (
    <Box sx={{ width: "100%", height: "100%", paddingTop: 8, justifyContent: "center", alignItems: "center" }}>
      <VideoList videos={videos} />
    </Box>
  );
};

export default DashBoardBody;
