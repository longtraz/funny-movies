export interface Video {
  url: string;
  title: string;
  source: string;
  description: string;
}
